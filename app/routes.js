const { exchangeRates } = require("../src/util.js");
const express = require("express");
const { request } = require("chai");
const router = express.Router();

router.get("/rates", (req, res) => {
  return res.status(200).send(exchangeRates);
});

router.post("/currency", (req, res) => {
  let foundExRate = exchangeRates.find((ex) => {
    return ex.alias === req.body.alias;
  });
  if (!req.body.hasOwnProperty("name")) {
    return res.status(400).send({
      error: "bad request - missing required parameter NAME",
    });
  }
  if (typeof req.body.name !== "string") {
    return res.status(400).send({
      error: "bad request - Name is not a string",
    });
  }
  if (req.body.name === "") {
    return res.status(400).send({
      error: "bad request - Name is empty string",
    });
  }
  if (!req.body.hasOwnProperty("ex")) {
    return res.status(400).send({
      error: "bad request - missing required parameter ex",
    });
  }
  if (typeof req.body.ex !== "object") {
    return res.status(400).send({
      error: "bad request - ex is not an object",
    });
  }
  if (Object.getOwnPropertyNames(req.body.ex).length === 0) {
    return res.status(400).send({
      error: "bad request - ex is empty object",
    });
  }
  if (!req.body.hasOwnProperty("alias")) {
    return res.status(400).send({
      error: "bad request - missing required parameter ALIAS",
    });
  }
  if (typeof req.body.alias !== "string") {
    return res.status(400).send({
      error: "bad request - Alias is not a string",
    });
  }
  if (req.body.alias === "") {
    return res.status(400).send({
      error: "bad request - Alias is empty string",
    });
  }
  if (foundExRate) {
    return res.status(400).send({
      success: "bad request - Alias is duplicate",
    });
  }
  return res.status(200).send({
    success: "Post request successfully",
  });
});

module.exports = router;
